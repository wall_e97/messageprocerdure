package controller;

import entity.FileInfomation;
import entity.MessageInProcess;
import entity.MessageSendResponse;
import entity.ValidateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import service.FileStorageService;
import service.FileValidator;
import service.SFileAttach;
import service.SProcedure;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

@Controller
public class CProcedure {
    @Value("${file.upload-dir}")
    private String RESPONSE_FOLDER;

    @Autowired
    private FileValidator fileVali;

    @Autowired
    private SProcedure sProcedure;

    @Autowired
    private SFileAttach fileAttach;

    @Autowired
    private FileStorageService fileStorageService;


    @PostMapping("/procedure/message")
    public MessageSendResponse sendMessage(@RequestBody MessageInProcess messageInModel, @RequestParam("file") MultipartFile file) {
        MessageSendResponse response = new MessageSendResponse(ValidateResult.success, "0", "", "");
        //check them 1 so dieu kien
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();
        FileInfomation fileInfomation = new FileInfomation();
        fileInfomation.setFileName(fileName);
        fileInfomation.setFileDownloadUri(fileDownloadUri);
        fileInfomation.setFileType(file.getContentType());
        fileInfomation.setSize(file.getSize());

        fileAttach.saveFileInfomation(fileInfomation);
        messageInModel.setFileattach_ids(fileInfomation.getId());
        sProcedure.saveMessage(messageInModel);
        return response;
    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.out.println(ex);
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}
