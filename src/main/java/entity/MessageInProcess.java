package entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "message_procedure")
public class MessageInProcess implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(name="description")
    private String description;

    @Column(name="document_type")
    private String document_type;

    @Column(name="from_unit_code")
    private String from_unit_code;

    @Column(name="to_unit_codes")
    private String to_unit_codes;

    @Column(name = "send_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date send_date;

    @Column(name="fileattach_ids")
    private Long fileattach_ids;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getFrom_unit_code() {
        return from_unit_code;
    }

    public void setFrom_unit_code(String from_unit_code) {
        this.from_unit_code = from_unit_code;
    }

    public String getTo_unit_codes() {
        return to_unit_codes;
    }

    public void setTo_unit_codes(String to_unit_codes) {
        this.to_unit_codes = to_unit_codes;
    }

    public Date getSend_date() {
        return send_date;
    }

    public void setSend_date(Date send_date) {
        this.send_date = send_date;
    }

    public Long getFileattach_ids() {
        return fileattach_ids;
    }

    public void setFileattach_ids(Long fileattach_ids) {
        this.fileattach_ids = fileattach_ids;
    }
}
