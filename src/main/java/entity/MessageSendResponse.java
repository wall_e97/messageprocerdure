package entity;

public class MessageSendResponse extends ModelResponse{
    private String message_id;

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public MessageSendResponse() {
        super();
        // TODO Auto-generated constructor stub
    }

    public MessageSendResponse(String status, String errorCode, String errorDesc, String messageId) {
        super(status, errorCode, errorDesc);
        this.message_id = messageId;
        // TODO Auto-generated constructor stub
    }
}
