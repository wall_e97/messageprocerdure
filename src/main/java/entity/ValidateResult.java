package entity;

public class ValidateResult {
    private String status;
    private String errorCode;
    private String message;
    public static final String success="SUCCESS";
    public static final String fail="FAIL";
    public static final String error1="From unit code not found";
    public static final String error2="To unit code not found";
    public static final String error3="Unit Code does not exist";
    public static final String error4="Source ID not found";
    public static final String error5="Ref code not found";
    public static final String error6="Procedures code not found";
    public static final String error7="Creation date not found";
    public static final String error8="Creation date not the format yyyy/MM/dd HH:mm:ss";
    public static final String error9="Data eform not found";
    public static final String error10="Accept Date not found";
    public static final String error11="The status field value is incorrect";
    public static final String error12="Profile Id does not exist";
    public static final String error13="Source code does not exist";
    public static final String error14="data_eform is not base64 encoded";
    public static final String error15="Data not exist";
    public static final String error16="profile_id(Incorrect data type)";
    public static final String error17="The type value is incorrect";
    public static final String error18="The type of transmission does not correspond to the data";
    public static final String error19="Id input not found";
    public static final String error20="Id does not exist";
    public static final String error21="No data could be found for the given fields";

    public static final String error22="Form Unit Code does not exist";
    public static final String error23="To Unit Code does not exist";
    public static final String error24="Profile already exist";


    public static final String error25="source_id not be null";
    public static final String error26="title not be null";
    public static final String error27="image_url not be null";
    public static final String error28="publish_date not be null";
    public static final String error29="content not be null";
    public static final String error30="organize not be null";
//	public static final String error31="type not be null";

    public static final String error32="title must not exceed 250 characters";
    public static final String error33="description must not exceed 500 characters";
    public static final String error34="image_url must not exceed 500 characters";
    public static final String error35="category must not exceed 250 characters";
    public static final String error36="organize must not exceed 250 characters";
    public static final String error37="publish_date not the format yyyy/MM/dd HH:mm:ss";
    public static final String error38="content must be base64 encoded";
    public static final String error171="type in data false";

    public static final String error39="name Owner not be null";
    public static final String error40="name Applicant not be null";

    public static final String error41="status_profile value is incorrect";
    public static final String error42="profile not found";
    public static final String error43="type not be null";

    public static final String error44="Process_officials not be null";
    public static final String error45="Time_of_process not be null";
    public static final String error46="Over limit received";

    public static final String error47=" not the format yyyy/MM/dd HH:mm:ss";
    public static final String error48=" not found";
    public static final String error49=" not null";
    public static final String error50=" Please select a file to upload";
    public static final String error51=" Unknownd Exception";

    public static final String error52="  Please select a file.";
    public static final String error53=" Invalidate file type";
    public static final String error54=" File exceeded size";
    public static final String error55=" Form Unit code not validate";
    public static final String error56=" Content is not base64 encoded";
    public static final String error57=" File Attach Url not validate";

    public static final String error58=" form_unit_code not be null";
    public static final String error59=" to_unit_code not be null";
    public static final String error60=" profiles_status not be null";

    public static final String error61="is_request_payment value is incorrect";
    public static final String error62="is_request_payment not be null";
    public static final String error63="total_amount not be null";
    public static final String error64="is_update value is incorrect";

    public static final String error65="Profile not available for unit: ";




    public ValidateResult(String status, String errorCode, String message) {
        super();
        this.status = status;
        this.errorCode = errorCode;
        this.message = message;
    }


    public ValidateResult() {
        super();
        // TODO Auto-generated constructor stub
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
