package entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelResponse {
    String status;
    //@JsonProperty("ErrorCode")
    String errorCode;
    //@JsonProperty("ErrorDesc")
    String errorDesc;

    public ModelResponse() {};

    public ModelResponse(String status, String errorCode, String errorDesc ) {
        this.status = status;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
}
