package procedure;

import entity.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class ProcedureApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProcedureApplication.class, args);
    }

}
