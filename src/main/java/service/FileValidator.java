package service;

import entity.ValidateResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

public class FileValidator {
    public static final String[] PNG_MIME_TYPE = {"image/png","image/jpg","image/gif","edxml", MimeTypes.MIME_APPLICATION_PDF,
            MimeTypes.MIME_APPLICATION_MSWORD, MimeTypes.MIME_APPLICATION_X_TEX, MimeTypes.MIME_APPLICATION_ZIP,
            MimeTypes.MIME_APPLICATION_X_RAR_COMPRESSED, MimeTypes.MIME_TEXT_RICHTEXT, MimeTypes.MIME_TEXT_PLAIN,
            MimeTypes.MIME_TEXT_RTF,MimeTypes.MIME_APPLICATION_VND_MSEXCEL,MimeTypes.MIME_APPLICATION_MATHML_XML,MimeTypes.MIME_APPLICATION_VND_MOZZILLA_XUL_XML
            ,MimeTypes.MIME_APPLICATION_X_GZIP,MimeTypes.MIME_APPLICATION_XSLT_XML,MimeTypes.MIME_TEXT_VND_WAP_XML,MimeTypes.MIME_TEXT_X_SETEXT,
            MimeTypes.MIME_APPLICATION_RDF_XML,MimeTypes.MIME_APPLICATION_JSON,MimeTypes.MIME_APPLICATION_OCTET_STREAM};

    public static final long TEN_MB_IN_BYTES = 10485760*10;

}
