package service;

import entity.MessageInProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reponsitory.RProcedure;

@Service
public class SProcedure {
    @Autowired
    private RProcedure rProcedure;

    public void saveMessage(MessageInProcess messageInProcess) {
        rProcedure.save(messageInProcess);
    }
}
