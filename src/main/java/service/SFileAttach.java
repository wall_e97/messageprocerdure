package service;

import entity.FileInfomation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reponsitory.RFileAttach;

@Service
public class SFileAttach {
    @Autowired
    private RFileAttach fileAttach;

    public void saveFileInfomation(FileInfomation fileInfomation) {
        fileAttach.save(fileInfomation);
    }
}
